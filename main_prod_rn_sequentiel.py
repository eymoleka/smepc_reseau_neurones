from sklearn.model_selection import train_test_split
import Read_Instances as RI # Contient les fonctions qui permettent de lire les fichiers contenant les instances
import Sequential_Model as SM
import tensorflow as tf
import Tuning as KTA
import Functional_model as FM
import numpy as np
import Analyse_PCA_data as PCA
import sys
#import Al_He_Functional_model_production as FMP
import Al_He_Functional_model_production as CL
from collections import Counter
import matplotlib.pyplot as plt
import Alternative_learning_scheme as AFMP
#min_num_inst = 70
#max_num_inst = 2621#debug 200
min_num_inst = 10000##
max_num_inst = 15999##
tf.random.set_seed(42)
#nombre_d_instance =  max_num_inst - min_num_inst
#========================================================
# I.
#========================================================

#Lecture des instances
#Test fonction
#lines = RI.Read_Sequential("InstanceReseauNeurones/RN_instance_prod__", min_num_inst)
#rend, CoutFixe, CoutVar, CoutProd, DateDebLastRecharg, QteRecharg = RI.Read_Line_By_Line("InstanceReseauNeurones/RN_instance_prod__", 70)
#x_data, y_data = RI.Input_Output_RN_By_Type("InstanceReseauNeurones/RN_instance_prod__", min_num_inst)
#x_data_p, y_data_p = RI.Input_Output_RN_Period_By_Period("InstanceReseauNeurones/RN_instance_prod__", min_num_inst)
X_data, Y_data= RI.X_data_Y_data_construct(min_num_inst, max_num_inst, 1,"InstanceReseauNeurones/RN_instance_prod__")#en mettant 1 ou 0 cela permet de classé
                                                                                                                     #les données par type ou par période
    #si choix_type_lecture = 0 alors on utilise la transformation en fichier en vecteur "par type"
    #si choix_type_lecture = 1 alors on utilise la transformation en fichier en vecteur "periode par periode

#print(min(Y_data))
#print(max(Y_data))
#calcul de la taille de la plus grande liste de X_data
Max_tail_list=0
for i in range(0, len(X_data)):
    if(len(X_data[i]) > Max_tail_list):
        Max_tail_list=len(X_data[i])

#on transforme chaque liste de X_data en liste de taille Max_tail_list en completant la liste pas des 0
for i in range(0, len(X_data)):
    if(len(X_data[i]) < Max_tail_list):
        list_0 = [0 for i in range(Max_tail_list-len(X_data[i]))]
        X_data[i].extend(list_0)
X_train_me=np.asarray(X_data[90:])
y_train_me=np.asarray(Y_data[90:])
X_test_me=np.asarray(X_data[0:90])
y_test_me=np.asarray(Y_data[0:90])
##diviser en données de test et données d'entrainement
#X_train, X_test, y_train, y_test = train_test_split(X_data, Y_data, test_size=0.33, random_state=42)

#X_train_and_test = X_train+X_test
#y_train_test=y_train+y_test
#X_train=X_train_and_test[90:]
#X_test=X_train_and_test[0:90]
#y_train=y_train_test[90:]
#y_test=y_train_test[0:90]
##transformer la liste de listes en tenseurs
##Y_data_tensor = tf.convert_to_tensor(y_train)
##X_data_tensor = tf.convert_to_tensor(X_train)
##Y_data_test_tensor = tf.convert_to_tensor(y_test)
#X_data_test_tensor = tf.convert_to_tensor(X_test)
#X_data_train_tensor = tf.convert_to_tensor(X_train)
#========================================================
# II. Modèle Séquentiel 21/02/2022
#========================================================

# création d'un modèle sans tuning (les parties qui sont commentées #...# sont à décommenter si on veut les exécuter
#list_fct_act=[['softplus','softplus'],['sigmoid','sigmoid'], ['linear','linear'], ['tanh','tanh'],['elu', 'elu'],['relu','relu'],['selu', 'selu'],['selu', 'relu'],['relu', 'selu']]#, ['relu','sigmoid'], ['relu','linear'], ['sigmoid','relu'], ['linear','relu'],['tanh', 'relu'], ['tanh','linear'],['tanh','sigmoid'],['elu', 'relu'], ['elu','linear'],['elu','sigmoid'],['selu', 'relu'], ['selu','linear'],['selu','sigmoid'],['relu', 'elu']
#cheminComplet = "SequentialModel_comparaison_fct_activation.txt"
#for cpt_fct_act in list_fct_act:
##construction du modèle RN
#    fct_act_couche_1=cpt_fct_act[0]
#    fct_act_couche_2=cpt_fct_act[1]
#    model = SM.MLP_LN_regression_construct(Max_tail_list,fct_act_couche_1,fct_act_couche_2)#

##compilation et entrainement du modèle RN
#    SM.MLP_LN_regression_FctError_Apprentissag_SaveModel(model, 'adam', 'mean_squared_error', X_train_me, y_train_me, 200, 32, 0)#

##prediction
##test sur les instances d'apprentissage
#    err_train= SM.MLP_LN_regression_predire(len(X_train_me), X_train_me, y_train_me,'SequentialModel_prediction_train_by_period.pdf',200)#
##test sur les instances de test
#    err_test= SM.MLP_LN_regression_predire(len(X_test_me), X_test_me, y_test_me, 'SequentialModel_prediction_test_by_period.pdf',200)#
##ecrire le résultat dans un fichier
#    with open(cheminComplet, "a") as fichier_RS:
#        text_sortie = str(cpt_fct_act)+ "err train : "+ str(err_train)+ "err test : "+ str(err_test)+"\n"
#        print(text_sortie)
#        fichier_RS.write(text_sortie)


#========================================================
#Le code ci-dessous sert à exécuter MLP_LN_regression_FctError_Apprentissag_SaveModel une certains nombre de fois égale au nombre d'epochs voulu.
# On le fait juste pour pouvoir calculer les gaps des données de test et d'entrainement à chaque epochs pour déssiner une courbe de l'évolution des gaps au fil des epochs
#========================================================

fct_act_couche_1='selu'
fct_act_couche_2='selu'
list_err_test=[]
list_err_train=[]
#Nb_epochs=9#calculé avec callback (par type)
#Nb_epochs=32#calculé avec callback (par periode)
#Nb_epochs=20#valeur donnant le plus petit gap test (par type) 205
Nb_epochs=2#valeur donnant le plus petit gap test  (par periode) 205
i_min_gap_test=1
min_err_test=2000000
for i in range(1,Nb_epochs+1):
    print("itération : ", i)
    model = SM.MLP_LN_regression_construct(Max_tail_list,fct_act_couche_1,fct_act_couche_2)#
    err_test, err_train=SM.MLP_LN_regression_construct_gap_by_epoch(model,'adam', 'mean_squared_error', X_train_me, y_train_me, X_train_me, y_train_me,X_test_me, y_test_me, i, 32, 0)
    list_err_test.append(err_test)
    list_err_train.append(err_train)
    if(err_test<min_err_test):
        i_min_gap_test=i
        min_err_test=err_test
print("SIMPLE_ l'itération avec le meilleur gap test est : ", i_min_gap_test)
x = np.linspace(1,Nb_epochs,Nb_epochs) #on a N valeurs sur l'axe des x (une pour chaque vecteur)
axes = plt.axes()
#axes.set_xlim([4, 8])
axes.set_ylim([0, 200])
plt.plot(x, list_err_train, "b", label='erreur sur les données d\'apprentissage' )
plt.plot(x, list_err_test, "r", label='erreur sur les données de test')
plt.ylabel('Moyenne des gaps(%)')
plt.xlabel('nombre d\'itérations')
plt.legend()
plt.savefig('Gap_PROD_prediction_sequentialModel_test_valid_data__by_period.pdf')
plt.show()
