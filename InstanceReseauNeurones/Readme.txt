Ce document explique la signification de chaque ligne des fichiers de ce dossier :

10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 : rendements sur chaque période
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 : cout fixe sur chaque période
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 : cout variable sur chaque période
5 : cout de production de la solution optimale
36 : date de debut de la dernière recharge de la solution optimale
0 0 0 0 0 12 12 12 12 22 22 22 22 0 0 : quantité d'hydrogène à recharger sur chaque période de la solution heuristique (valeurs mu_q)