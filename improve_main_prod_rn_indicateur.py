#Dans ce fichier on va construire et entrainer le réseau de neurones qui va prédire le coût de production (15/04/2022)
import tensorflow as tf
import Alternative_learning_scheme as AFMP
import numpy as np
import matplotlib.pyplot as plt
min_num_inst = 10000##
max_num_inst = 15999##
tf.random.set_seed(42)

#On va calcule les indicateurs et récupérer les entrées du réseaux et ajouter min max moy ecartType
P_A, P0_A, G_A, C_A, A, P__N, P0_P, A_PR, V0_V, C, S_N, One, in_P_moins_P0, in_P_plus_P0, out_Cost_A_P, in_K, in_mu = AFMP.improve_Input_output_Cost_value("instances_avec_p/instance__", "InstanceReseauNeurones/RN_instance_prod__","InstanceReseauNeurones_suite/RN_instance_prod_suite__","InstanceReseauNeurones_suite_suite/RN_instance_prod_suite_suite__", min_num_inst, max_num_inst)

list_err_test=[]
list_err_train=[]
#Nb_epochs=205 #calculé avec la fonction callback
Nb_epochs=201 #valeur donnant le plus petit gap test #205
i_min_gap_test=1
min_err_test=2000000
for i in range(1,Nb_epochs+1):
    print("itération : ", i)
    #On construit le réseau
    #On entraine le réseau
    #On calcule erreur test et entrainement
    err_test, err_train=AFMP.improve_Cost_value_model_fig_gap_by_epoch(i,P_A, P0_A, G_A, C_A, A, P__N, P0_P, A_PR, V0_V, C, S_N, One, in_P_moins_P0, in_P_plus_P0, out_Cost_A_P, in_K, in_mu)
    #if(i==200):
        #faire les courbes des valeurs predites et opt des données de test et d'entrainement
    list_err_test.append(err_test)
    list_err_train.append(err_train)
    if(err_test<min_err_test):
        i_min_gap_test=i
        min_err_test=err_test
print("IND_COUT l'itération avec le meilleur gap test est : ", i_min_gap_test)
x = np.linspace(1,Nb_epochs,Nb_epochs) #on a N valeurs sur l'axe des x (une pour chaque vecteur)
axes = plt.axes()
#axes.set_xlim([4, 8])
axes.set_ylim([0, 50])
plt.plot(x, list_err_train, "b", label='erreur sur les données d\'apprentissage' )
plt.plot(x, list_err_test, "r", label='erreur sur les données de test')
plt.ylabel('Moyenne des gaps(%)')
plt.xlabel('nombre d\'itérations')
plt.legend()
plt.savefig('6000_PROD_loss_reseauALternativeLearning2.pdf')
plt.show()
